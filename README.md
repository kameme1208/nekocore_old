# NekoCore
(๑•̀ω•́๑)ライブラリ置き場

## 機能一覧
- RayUtils - **ブロック/エンティティ用 衝突判定ライブラリ、速度よりかはフィルタ重視(速度も頑張った？)**
  - Ray                 - RayHitのイテレータを保持するクラス
  - RayBuilder          - Rayを作成するためのビルダークラス、ブロックやエンティティの高度なフィルタ機能も提供
  - BlockPos            - Block座標を保持するためのクラス、最小限の機能
  - BlockPosIterator    - 2点間で通過するブロック座標を求めるためのクラス、割と高速(100m,最速(横)平均25us,最遅(斜め)平均35us)
----
- TagUtils - **NBTタグを扱うためのライブラリ、かめ系プラグインで結構使って熟成した**
  - TagCompound     - **マップ階層を扱うための定義**
    - TagSection    - TagCompoundの実装
  - TagCollection   - **配列(コレクション)を扱うための定義**
    - TagList       - TagCollectionの実装
  - TagValue        - **値を扱うための定義**
    - TagString
    - TagNumber
      - TagByte
      - TagShort
      - TagInteger
      - TagLong
      - TagFloat
      - TagDouble
----
- BlockObject - **表示だけのアーマースタンド(パケットオンリー)を扱うライブラリ**
- ActionInventory - **インベントリ操作でアイテムに登録した処理実行するインベントリを扱うライブラリ**
