package kame.nekocore.nbt.interfaces;

/**
 * このインターフェースはNBTタグの最低限の機能を提供するインターフェースです。
 * @author kameme
 */
public interface TagBase {

	/**
	 * 指定されたキーがこのマップに含まれている場合にtrueを返します。
	 * (このようなマッピングは1つのみ存在できます。)
	 * @return この値が含まれていた場合true
	 */
	public boolean hasValue();

	
	/**
	 * この値に格納されている要素のクラスを取得します。
	 * @return 関連付けられている要素のクラス
	 */
	public Class<? extends TagBase> getType();

}
