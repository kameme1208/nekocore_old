package kame.nekocore.nbt.interfaces;

/**
 * このインターフェースはNBTタグの書き込み機能を提供するインターフェースです。
 * このインターフェースを実装するクラスはsaveメソッドを使用してNBTタグを何らかの場所に書き込む機能を定義します。
 * @author kameme
 */
public interface TagWriteable {

	/**
	 * 現在の値を変更元のインスタンス(ItemStack,Entity,BlockStateなど)に変更を適用させます。<br>このメソッドは変更元のインスタンスが同じであればどの階層から行っても全ての変更が適用されます。
	 * つまり<br><b>変更前</b>{Pets:{A:{type:"cat",name:"鳥"},B:{type:"dog",name:"虫"}}} (A,Bの両方を変更しBの階層でsaveを実行)
	 * <br><b>変更後</b>{Pets:{A:{type:"cat",name:"猫"},B:{type:"dog",name:"犬"}}}
	 * <br>のように、どこのTagSectionでsaveを実行しても全ての変更が保存されます。
	 */
	public void save();

}
