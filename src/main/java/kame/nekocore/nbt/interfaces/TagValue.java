package kame.nekocore.nbt.interfaces;

/**
 * このインターフェースはNBTタグの値を扱う機能を提供するインターフェースです。
 * @author kameme
 */
public interface TagValue extends TagBase {

	/**
	 * このNBTが持っている値をbooleanに変換して返します。
	 * @return boolean型に変換した値
	 */
	public boolean getBoolean();

	/**
	 * このNBTが持っている値をbooleanに変換して返します。
	 * @return boolean型に変換した値 */
	public default boolean booleanValue() { return getBoolean(); }

	/**
	 * このNBTが持っている値をbyteに変換して返します。
	 * @return byte型に変換した値
	 */
	public byte byteValue();

	/**
	 * このNBTが持っている値をshortに変換して返します。
	 * @return short型に変換した値
	 */
	public short shortValue();

	/**
	 * このNBTが持っている値をintに変換して返します。
	 * @return int型に変換した値
	 */
	public int intValue();

	/**
	 * このNBTが持っている値をlongに変換して返します。
	 * @return long型に変換した値
	 */
	public long longValue();

	/**
	 * このNBTが持っている値をfloatに変換して返します。
	 * @return float型に変換した値
	 */
	public float floatValue();

	/**
	 * このNBTが持っている値をdoubleに変換して返します。
	 * @return double型に変換した値
	 */
	public double doubleValue();

	/**
	 * このNBTにboolean型の値をセットします。
	 */
	public void set(boolean value);

	/**
	 * このNBTにbyte型の値をセットします。
	 */
	public void set(byte value);

	/**
	 * このNBTにshort型の値をセットします。
	 */
	public void set(short value);

	/**
	 * このNBTにint型の値をセットします。
	 */
	public void set(int value);

	/**
	 * このNBTにlong型の値をセットします。
	 */
	public void set(long value);

	/**
	 * このNBTにfloat型の値をセットします。
	 */
	public void set(float value);

	/**
	 * このNBTにdouble型の値をセットします。
	 */
	public void set(double value);

	/**
	 * このNBTにString型の値をセットします。
	 */
	public void set(String value);

	/**
	 * このNBTが持っている値をNumber型に変換して返します。
	 * @return Number型に変換した値
	 */
	public Number number();

	/**
	 * このNBTが持っている値を文字列として返します。
	 * @return NBTが持つ文字列
	 */
	public String toString();
}
