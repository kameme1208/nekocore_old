package kame.nekocore.nbt.interfaces;

import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;

import kame.nekocore.nbt.TagSource;

/**
 * このインターフェースはNBTタグの階層を扱う機能を提供するインターフェースです。
 * @author kameme
 */
public interface TagCompound extends TagBase {
	
	/**
	 * このマップに指定された要素が全て含まれているかどうかを返します。
	 * @param other - 含まれているかどうか検索する要素
	 * @return 指定された要素を全て含む場合はtrue
	 */
	public boolean hasAll(TagCompound other);

	/**
	 * このマップに指定された要素が全て含まれているかどうかを返します。
	 * @param other - 含まれているかどうか検索する要素
	 * @param predicate - オブジェクトの比較時に使用するBiPredicate
	 * @return 指定された要素を全て含む場合はtrue
	 */
	public boolean hasAll(TagCompound other, BiPredicate<? extends TagBase, ? extends TagBase> predicate);
	
	/**
	 * 指定されたキーがこのマップに含まれている場合にtrueを返します。
	 * (このようなマッピングは1つのみ存在できます。)
	 * @param tagName - このマップ内にあるかどうかが判定されるキー
	 * @return 指定されたキーに値が含まれていた場合true
	 */
	public boolean hasTag(String tagName);

	/**
	 * 指定されたキーがこのマップに含まれており、かつ指定クラスにキャストできる場合にtrueを返します。
	 * @param tagName - このマップ内にあるかどうかが判定されるキー
	 * @param classFilter - 判定されるキーのクラス
	 * @return 指定されたキーに値が含まれており、かつ指定クラスにキャストできる場合true
	 */
	public boolean hasTag(String tagName, Class<? extends TagBase> classFilter);

	/**
	 * 指定されたキーがマップされている値を返します。このマップにそのキーのマッピングが含まれていない、または指定のクラスと一致しない場合はdefaultValueで渡された値を返します。
	 * @param tagName - 関連付けられた値が返されるキー
	 * @param classFilter - 関連付けられるキーの指定クラス
	 * @param defaultValue - 関連付けられた要素が無かったときに返す値
	 * @return 指定されたキーがマップされている値。このマップにそのキーのマッピングが含まれていない場合はdefaultValue
	 */
	public <T extends TagSource & TagBase> T get(String tagName, Class<T> classFilter, T defaultValue);

	/**
	 * 指定したキーに新しくNBTの値を作成します。
	 * 既に値が存在しクラスにキャストが出来る場合はその値を返します。
	 * @param tagName - NBTの階層を作成するキー値
	 * @param cls - 作成する階層のクラス
	 * @return 新しく作成したNBTの階層
	 */
	public <T extends TagSource & TagBase> T create(String tagName, Class<T> cls);
	
	/**
	 * このNBTの指定されたキーに要素を格納します。
	 * 指定したキーに値を格納します。
	 * @param tagName - 格納するキーの値
	 * @param nbt - 格納する要素の値
	 */
	public void set(String tagName, TagBase nbt);

	/**
	 * このNBTの要素を渡されたNBTの要素で置き換えます。
	 * @param nbt - 置き換えるNBTの要素
	 */
	public void set(TagCompound nbt);

	/**
	 * 指定されたキーがこのNBTの要素にあれば、その最初のものをNBTの要素から削除します。その要素がない場合、変更はありません。
	 * @param tagName - このNBTの要素から削除される要素(その要素が存在する場合)
	 * @return 指定されたキーに含まれていた値、値が存在していなければnull
	 */
	public TagBase remove(String tagName);
	
	/**
	 * 指定されたキーがこのNBTの要素にあれば、その最初のものをNBTの要素から削除します。その要素がない場合、変更はありません。
	 * @param tagName - このNBTの要素から削除される要素(その要素が存在する場合)
	 * @param cls - 削除する階層のクラス
	 * @return 指定されたキーに含まれていた値、値が存在していなければnull
	 */
	public <T extends TagSource & TagBase> T remove(String tagName, Class<T> cls);
	
	/**
	 * 指定されたキーに格納されている要素のクラスを取得します。
	 * @param tagName - 関連付けられた値のクラスが返されるキー
	 * @return 関連付けられている要素のクラス、値が存在していなければVoid.class
	 */
	public Class<? extends TagBase> getType(String tagName);

	/**
	 * このマップに含まれるキーのSetビューを返します。
	 * セットはマップと連動しているので、マップに対する変更はセットに反映され、また、セットに対する変更はマップに反映されます。
	 * セットの反復処理中にマップが変更された場合、反復処理の結果は定義されていません(イテレータ自身のremoveオペレーションを除く)。
	 * セットは要素の削除をサポートします。Iterator.remove、Set.remove、removeAll、retainAll、およびclearオペレーションで対応するマッピングをマップから削除します。
	 * addまたはaddAllオペレーションはサポートしていません。
	 * @return マップに含まれているキーのセット・ビュー
	 */
	public Set<String> keySet();

	/**
	 * このNBTの階層がキーと値のマッピングを保持しない場合にtrueを返します。
	 * @return このマップがキーと値のマッピングを保持しない場合はtrue
	 */
	public default boolean isEmpty() { return keySet().isEmpty(); }

	/**
	 * このNBTの階層内の要素数を返します。NBTの階層内にInteger.MAX_VALUEより多くの要素がある場合は、Integer.MAX_VALUEを返します。
	 * @return このNBTの階層内の要素数
	 */
	public default int size() { return keySet().size(); }

	/**
	 * このNBTが持っている要素をマップに変換して返します。
	 * @return このNBTが持っているすべての要素
	 */
	public Map<String, Object> toMap();
}
