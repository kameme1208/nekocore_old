package kame.nekocore.nbt.interfaces;

import java.util.Collection;
import java.util.List;

/**
 * このインターフェースはNBTタグのコレクションを扱う機能を提供するインターフェースです。
 * @author kameme
 * @param <T> - コレクション内の要素のキャストできるスーパークラス
 */
public interface TagCollection<T extends TagBase> extends TagBase, List<T> {
	
	/**
	 * 指このリスト内の指定された位置にある要素を返します。
	 * @param index - 返される要素のインデックス
	 * @return このリスト内の指定された位置にある要素
	 */
	public T get(int index);
	
	/**
	 * 指定された要素をこのリストの最後に追加します。
	 * @param nbt - 追加する要素
	 * @return この要素に追加できた場合はtrueそうでない場合はfalse
	 */
	public boolean add(T nbt);
	
	/**
	 * このリスト内の指定された位置にある要素を削除します。
	 * @param index - 削除される要素のインデックス
	 * @return 指定された位置に以前あった要素
	 */
	public T remove(int index);

	/**
	 * 指定された要素がこのリストにあれば、その最初のものをリストから削除します。
	 * @param nbt - 削除する要素
	 * @return このコレクションから削除が完了した場合はtrue
	 */
	public boolean remove(Object nbt);
	
	/**
	 * 指定されたコレクション内のすべての要素を、指定されたコレクションのイテレータによって返される順序で、このリストの最後に追加します。
	 * @param c - このリストに追加される要素を含むコレクション
	 * @return  呼出しの結果としてこのリストが変更された場合はtrue
	 */
	public boolean addAll(Collection<? extends T> c);
	
	/**
	 * 指定されたコレクション内のすべての要素を、指定されたコレクションのイテレータによって返される順序で、このリストの最後に追加します。
	 * @param index - 指定されたコレクションの最初の要素を挿入する位置のインデックス
	 * @param c - このリストに追加される要素を含むコレクション
	 * @return  呼出しの結果としてこのリストが変更された場合はtrue
	 */
	public boolean addAll(int index, Collection<? extends T> c);
	
	/**
	 * 指定されたコレクションにも格納されているこのコレクションのすべての要素を削除します。
	 * @params c - このリストから削除される要素を含むコレクション
	 * @return 呼出しの結果としてこのリストが変更された場合はtrue
	 */
	public boolean removeAll(Collection<?> c);
	
	/**
	 * このコレクションにおいて、指定されたコレクションに格納されている要素だけを保持します。
	 * @params c - このリストで保持される要素を含むコレクション
	 * @return 呼出しの結果としてこのリストが変更された場合はtrue
	 */
	public boolean retainAll(Collection<?> c);
	
	/**
	 * このコレクションからすべての要素を削除します。
	 */
	public void clear();
	
	/**
	 * このリスト内の指定された位置にある要素を、指定された要素に置き換えます。
	 * @param index - 置換される要素のインデックス
	 * @param element - 指定された位置に格納される要素
	 * @return 指定された位置に以前あった要素
	 */
	public T set(int index, T element);
	
	/**
	 * このリスト内の指定された位置に、指定された要素を挿入します。
	 * @param index - 指定の要素が挿入される位置のインデックス
	 * @param element - 挿入される要素
	 */
	public void add(int index, TagBase element);
}
