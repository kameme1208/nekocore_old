package kame.nekocore.block;

import java.util.Arrays;
import java.util.Iterator;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

public class BlockPosIterator implements Iterator<BlockPos> {

	private final int distance;
	private static final int gridSize = 1 << 24;
	private boolean end = false;
	
	private int[][] queue = new int[3][3];
	private int cnt = -1;
	private int cur = 0;
	private int cap;
	
	private long[] move = { 0, 0 };
	private long[] step = { 0, 0 };

	private BlockFace[] face = new BlockFace[3];
	
	public BlockPosIterator(Location start, Location end) {
		this(start.toVector(), end.toVector());
	}
	
	public BlockPosIterator(Vector start, Vector end) {
		this(start, end.clone().subtract(start), start.distanceSquared(end));
	}
	
	public BlockPosIterator(Location loc, double distance) {
		this(loc.toVector(), loc.getDirection(), distance);
	}

	public BlockPosIterator(Vector start, Vector vec, double distance) {
		this.distance = (int)distance;

		int[] first = { floor(start.getX()), floor(start.getY()), floor(start.getZ()) };
		
		double[] len = { Math.abs(vec.getX()), Math.abs(vec.getY()), Math.abs(vec.getZ()) };
		double[] dir = { Math.max(len[1], len[2]), Math.max(len[0], len[2]), Math.max(len[0], len[1]) };
		double[] pos = { 0, 0, 0 };
		
		int f = len[0] >= dir[0] ? 0 : len[1] > dir[1] ? 2 : 1;
		int s = len[0] >= dir[0] ? 1 : len[1] > dir[1] ? 0 : 2;
		int t = len[0] >= dir[0] ? 2 : len[1] > dir[1] ? 1 : 0;

		dir[f] = len[0];
		dir[s] = len[1];
		dir[t] = len[2];
		
		face[f] = vec.getX() > 0 ? BlockFace.EAST : BlockFace.WEST;
		face[s] = vec.getY() > 0 ? BlockFace.UP : BlockFace.DOWN;
		face[t] = vec.getZ() > 0 ? BlockFace.SOUTH : BlockFace.NORTH;

		pos[f] = getPosition(vec.getX(), start.getX(), first[0]);
		pos[s] = getPosition(vec.getY(), start.getY(), first[1]);
		pos[t] = getPosition(vec.getZ(), start.getZ(), first[2]);

		move[0] = floor((pos[1] - dir[1] * (pos[0] / dir[0])) * gridSize);
		step[0] = round(dir[1] / dir[0] * gridSize);
		move[1] = floor((pos[2] - dir[2] * (pos[0] / dir[0])) * gridSize);
		step[1] = round(dir[2] / dir[0] * gridSize);

		if(move[0] + step[0] <= 0) { move[0] = -step[0] + 1; }

		if(move[1] + step[1] <= 0) { move[1] = -step[1] + 1; }

		queue[0][0] = first[0] - face[0].getModX();
		queue[0][1] = first[1] - face[0].getModY();
		queue[0][2] = first[2] - face[0].getModZ();
		
		if(move[0] < 0) {
			queue[0][0] -= face[1].getModX();
			queue[0][1] -= face[1].getModY();
			queue[0][2] -= face[1].getModZ();
		} else {
			move[0] -= gridSize;
		}

		if(move[1] < 0) {
			queue[0][0] -= face[2].getModX();
			queue[0][1] -= face[2].getModY();
			queue[0][2] -= face[2].getModZ();
		} else {
			move[1] -= gridSize;
		}
		
		scan();

		boolean startBlockFound = false;

		for(int count = cnt; count >= 0; count--) {
			if(Arrays.equals(queue[count], first)) {
				cnt = count;
				startBlockFound = true;
				break;
			}
		}

		if(!startBlockFound) { throw new IllegalStateException("Start block missed in BlockIterator"); }

		cap = round(distance / (Math.sqrt(dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2]) / dir[0]));
	}

	private double getPosition(double direction, double position, int blockPosition) {
		return direction > 0 ? (position - blockPosition) : (blockPosition + 1 - position);
	}
	
	public boolean hasNext() {
		scan();
		return cnt != -1;
	}

	public BlockPos next() {
		scan();
		return cnt <= -1 ? null : new BlockPos(queue[cnt][0], queue[cnt][1], queue[cnt--][2]);
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

	private void scan() {
		if(cnt >= 0) { return; }
		if(distance != 0 && cur > cap) {
			end = true;
			return;
		}
		if(end) { return; }

		cur++;

		move[0] += step[0];
		move[1] += step[1];

		if(move[0] > 0 && move[1] > 0) {
			queue[2][0] = queue[0][0] + face[0].getModX();
			queue[2][1] = queue[0][1] + face[0].getModY();
			queue[2][2] = queue[0][2] + face[0].getModZ();
			if(step[0] * move[1] < step[1] * move[0]) {
				queue[1][0] = queue[2][0] + face[1].getModX();
				queue[1][1] = queue[2][1] + face[1].getModY();
				queue[1][2] = queue[2][2] + face[1].getModZ();
				queue[0][0] = queue[1][0] + face[2].getModX();
				queue[0][1] = queue[1][1] + face[2].getModY();
				queue[0][2] = queue[1][2] + face[2].getModZ();
			}else {
				queue[1][0] = queue[2][0] + face[2].getModX();
				queue[1][1] = queue[2][1] + face[2].getModY();
				queue[1][2] = queue[2][2] + face[2].getModZ();
				queue[0][0] = queue[1][0] + face[1].getModX();
				queue[0][1] = queue[1][1] + face[1].getModY();
				queue[0][2] = queue[1][2] + face[1].getModZ();
			}
			move[0] -= gridSize;
			move[1] -= gridSize;
			cnt = 2;
			return;
		}else if(move[0] > 0) {
			queue[1][0] = queue[0][0] + face[0].getModX();
			queue[1][1] = queue[0][1] + face[0].getModY();
			queue[1][2] = queue[0][2] + face[0].getModZ();
			queue[0][0] = queue[1][0] + face[1].getModX();
			queue[0][1] = queue[1][1] + face[1].getModY();
			queue[0][2] = queue[1][2] + face[1].getModZ();
			move[0] -= gridSize;
			cnt = 1;
			return;
		}else if(move[1] > 0) {
			queue[1][0] = queue[0][0] + face[0].getModX();
			queue[1][1] = queue[0][1] + face[0].getModY();
			queue[1][2] = queue[0][2] + face[0].getModZ();
			queue[0][0] = queue[1][0] + face[2].getModX();
			queue[0][1] = queue[1][1] + face[2].getModY();
			queue[0][2] = queue[1][2] + face[2].getModZ();
			move[1] -= gridSize;
			cnt = 1;
			return;
		}else {
			queue[0][0] = queue[0][0] + face[0].getModX();
			queue[0][1] = queue[0][1] + face[0].getModY();
			queue[0][2] = queue[0][2] + face[0].getModZ();
			cnt = 0;
			return;
		}
	}

	protected static int floor(double d) {
		int i = (int)d;
		return d < i ? i - 1 : i;
	}

	public static int round(double num) {
		return floor(num + 0.5d);
	}
}
