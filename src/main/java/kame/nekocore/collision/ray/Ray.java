package kame.nekocore.collision.ray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import kame.nekocore.block.BlockPosIterator;
import kame.nekocorex.util.RayUtils.BlockRayHit;
import kame.nekocorex.util.RayUtils.EntityRayHit;
import kame.nekocorex.util.RayUtils.RayBuilderAbstract;

/**
 * このクラスは衝突判定の最低限の機能を提供する抽象クラスです。
 * @author kameme
 */
public class Ray implements Iterable<RayHit> {

	protected static final Comparator<RayHit> comparator = 
			(x, y) -> Double.compare(x.from.distanceSquared(x.hitloc), y.from.distanceSquared(y.hitloc));

	private static final Predicate<Block> Air = Block::isEmpty;
	private static final Predicate<Block> AirOrLiquid = Air.or(Block::isLiquid);

	private final List<RayHit> results = new ArrayList<>();

	/**
	 * Rayを飛ばすためのRayBuilderインスタンスを生成します
	 * @return - Rayを生成するためのRayBuilder
	 */
	public static RayBuilder builder() {
		return new WorldRayBuilder();
	}

	/**
	 * Block::isEmptyに相当するフィルターを返します。
	 * @return - 1つでも何かに衝突した場合はtrue
	 */
	public static final Predicate<Block> Air() {
		return Air;
	}

	/**
	 * Block::isEmpty || Block::isLiquid に相当するフィルターを返します。
	 * @return - 1つでも何かに衝突した場合はtrue
	 */
	public static final Predicate<Block> AirOrLiquid() {
		return AirOrLiquid;
	}

	@Override
	public Iterator<RayHit> iterator() {
		return results.iterator();
	}

	/**
	 * この結果に要素が1つも含まれていない場合にtrueを返します。
	 * @return - 1つでも何かに衝突した場合はtrue
	 */
	public boolean isEmpty() {
		return results.isEmpty();
	}

	private static class WorldRayBuilder extends RayBuilderAbstract {

		private WorldRayBuilder() {}

		@Override
		protected Ray traceBlock(World world, Vector from, Vector to, Entity entity, Map<Predicate<Block>, FilterType> filter) {
			Ray ray = new Ray();
			rayTraceBlock(ray.results, world, from, to, new BlockPosIterator(from, to));
			return check(ray, filter, Collections.emptyMap());
		}

		@Override
		protected Ray traceEntity(World world, Vector from, Vector to, Entity entity, Map<Predicate<Entity>, FilterType> filter) {
			Ray ray = new Ray();
			rayTraceEntity(ray.results, world, from, to, entity);
			return check(ray, Collections.emptyMap(), filter);
		}

		@Override
		protected Ray traceWorld(World world, Vector from, Vector to, Entity entity, Map<Predicate<Block>, FilterType> bfilter, Map<Predicate<Entity>, FilterType> efilter) {
			Ray ray = new Ray();
			rayTraceBlock(ray.results, world, from, to, new BlockPosIterator(from, to));
			rayTraceEntity(ray.results, world, from, to, entity);
			return check(ray, bfilter, efilter);
		}
		
		private Ray check(Ray ray, Map<Predicate<Block>, FilterType> block, Map<Predicate<Entity>, FilterType> entity) {
			Collections.sort(ray.results, (x, y) -> Double.compare(x.hitloc.distanceSquared(x.from), y.hitloc.distanceSquared(y.from)));
			Iterator<RayHit> each = ray.iterator();
			while(each.hasNext()) {
				switch(check(each.next(), block, entity)) {
				case STOP: while(each.next() != null) { each.remove(); } break;
				case SKIP: each.remove(); break;
				case INCLUDE:
				}
			}
			return ray;
		}
		
		private FilterType check(RayHit hit, Map<Predicate<Block>, FilterType> block, Map<Predicate<Entity>, FilterType> entity) {
			if(hit instanceof BlockRayHit) {
				for(Entry<Predicate<Block>, FilterType> filter : block.entrySet())if(filter.getKey().test((((BlockRayHit)hit).getBlock()))) return filter.getValue();
			}else if(hit instanceof EntityRayHit) {
				for(Entry<Predicate<Entity>, FilterType> filter : entity.entrySet())if(filter.getKey().test(((EntityRayHit)hit).getEntity())) return filter.getValue();
			}
			return FilterType.STOP;
		}
	}
}
