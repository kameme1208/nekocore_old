package kame.nekocore.collision.ray;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

/**
 * このクラスは衝突した部分の結果を示すための最低限の機能を提供する抽象クラスです。
 * @author kameme
 */
public abstract class RayHit {
	protected final Vector from;
	protected final Vector hitloc;
	private final Object hit;

	protected RayHit(Vector from, Vector hitloc, Object hit) {
		this.from = from;
		this.hitloc = hitloc;
		this.hit = hit;
	}

	/**
	 * このインスタンスの衝突した種類を返すメソッドです。
	 * @return - 衝突したものの種類(Entity, Block)
	 */
	public abstract HitType getType();
	

	/**
	 * 対象と衝突した位置を返すメソッドです。
	 * @return - 衝突した位置の正確な座標
	 */
	public Vector getPosition() {
		return hitloc;
	}

	/**
	 * 対象と衝突した位置を返すメソッドです。
	 * @return - 衝突した位置の正確な座標
	 */
	public abstract Location getLocation();

	/**
	 * 対象と衝突した面の向きを返すメソッドです。
	 * @return - 衝突した面
	 */
	public abstract BlockFace getDirection();
	
	/**
	 * 衝突したオブジェクトを指定したクラスへ変換が可能な場合、そのクラスへキャストして返します。
	 * @param filterClass
	 * @return - 指定クラスへのキャストに成功した場合、衝突したオブジェクト、それ以外はnull
	 */
	public <T> T getObject(Class<T> filterClass) {
		return filterClass.isInstance(hit) ? filterClass.cast(hit) : null;
	}

	public String toString() {
		return "Location:[x=" + hitloc.getX() + ", y=" + hitloc.getY() + ", z=" + hitloc.getZ() + "]";
	}
}