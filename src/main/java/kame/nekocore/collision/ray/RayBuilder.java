package kame.nekocore.collision.ray;

import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

/**
 * このインターフェースは衝突判定のクラスを作る最低限の機能を提供するインターフェースです
 * @author kameme
 */
public interface RayBuilder {

	/**
	 * 線上にいるBlockの判定をするフィルターを追加するメソッドです
	 * @param filter - 判定される関数
	 * @param type - フィルターの種類
	 * @return - 自身のインスタンス
	 */
	public RayBuilder addBlockFilter(Predicate<Block> filter, FilterType type);
	
	/**
	 * 標準設定のフィルターを追加適用するメソッドです
	 * フィルター内容: 空気ブロック以外と衝突したときに衝突したブロックを返す
	 * @return - 自身のインスタンス
	 */
	public RayBuilder addEntityFilter(Predicate<Entity> filter, FilterType type);
	
	/**
	 * 標準設定のフィルターを追加適用するメソッドです
	 * フィルター内容: 空気ブロック以外と衝突したときに衝突したブロックを返す
	 * @return - 自身のインスタンス
	 */
	public RayBuilder useTemplateFilter();
	/**
	 * PredicateがTrueを返すと、このオブジェクトを判定結果から除外するフィルターを追加します。
	 * @param filter - 判定される関数
	 * @return - 自身のインスタンス
	 */
	public RayBuilder skipBlock(Predicate<Block> filter);

	/**
	 * PredicateがTrueを返すと、このオブジェクトを判定結果に含むフィルターを追加します
	 * @param filter - 判定される関数
	 * @return - 自身のインスタンス
	 */
	public RayBuilder includeBlock(Predicate<Block> filter);

	/**
	 * PredicateがTrueを返すとそこでチェックを終了するフィルターを追加します
	 * @param filter - 判定される関数
	 * @return - 自身のインスタンス
	 */;
	public RayBuilder stopBlock(Predicate<Block> filter);

	/**
	 * PredicateがTrueを返すと、このオブジェクトを判定結果から除外するフィルターを追加します。
	 * @param filter - 判定される関数
	 * @return - 自身のインスタンス
	 */
	public RayBuilder skipEntity(Predicate<Entity> filter);

	/**
	 * PredicateがTrueを返すと、このオブジェクトを判定結果に含むフィルターを追加します
	 * @param filter - 判定される関数
	 * @return - 自身のインスタンス
	 */
	public RayBuilder includeEntity(Predicate<Entity> filter);

	/**
	 * PredicateがTrueを返すとそこでチェックを終了するフィルターを追加します
	 * @param filter - 判定される関数
	 * @return - 自身のインスタンス
	 */;
	public RayBuilder stopEntity(Predicate<Entity> filter);
	
	/**
	 * 現在の地点での衝突判定を判定します。
	 * @param from - 始点(getDirectionで方向を指定します)
	 * @param distance - 最大距離
	 * @return - 判定された結果を格納するクラス
	 */
	public Ray traceBlock(Location from, double distance);
	/**
	 * 現在の地点での衝突判定を判定します。
	 * @param from - 始点(getDirectionで方向を指定します)
	 * @param distance - 最大距離
	 * @return - 判定された結果を格納するクラス
	 */
	public Ray traceEntity(Location from, double distance);
	/**
	 * 現在の地点での衝突判定を判定します。
	 * @param from - 始点(getDirectionで方向を指定します)
	 * @param distance - 最大距離
	 * @return - 判定された結果を格納するクラス
	 */
	public Ray traceWorld(Location from, double distance);

	/**
	 * 現在の地点での衝突判定を判定します。
	 * @param entity - 視点の位置を取得するエンティティ
	 * @param distance - 最大距離
	 * @return - 判定された結果を格納するクラス
	 */
	public Ray traceBlock(LivingEntity entity, double distance);

	/**
	 * 現在の地点での衝突判定を判定します。
	 * @param entity - 視点の位置を取得するエンティティ
	 * @param distance - 最大距離
	 * @return - 判定された結果を格納するクラス
	 */
	public Ray traceEntity(LivingEntity entity, double distance);

	/**
	 * 現在の地点での衝突判定を判定します。
	 * @param entity - 視点の位置を取得するエンティティ
	 * @param distance - 最大距離
	 * @return - 判定された結果を格納するクラス
	 */
	public Ray traceWorld(LivingEntity entity, double distance);
}