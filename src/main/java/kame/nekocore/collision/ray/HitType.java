package kame.nekocore.collision.ray;

/**
 * RayHitインスタンスの衝突した種類を示すenum定数です
 * @author kameme
 */
public enum HitType {
	/** EntityRayHitクラスである場合はこの定数が返されます。*/
	ENTITY,
	/** BlockRayHitクラスである場合はこの定数が返されます。*/
	BLOCK
}