package kame.nekocore.collision.ray;


/**
 * フィルターの種類を示すenum定数です
 * @author kameme
 */
public enum FilterType {
	/** フィルターの判定がtrueである時は判定した対象を無視します */
	SKIP,
	/** フィルターの判定がtrueである時は判定した対象を結果に含ませます。*/
	INCLUDE,
	/** フィルターの判定がtrueである時は判定した対象を結果に含ませ、これより後ろにある対象は無視し判定処理を中止します。*/
	STOP
}