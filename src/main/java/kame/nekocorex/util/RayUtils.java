package kame.nekocorex.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import kame.nekocore.Main;
import kame.nekocore.block.BlockPos;
import kame.nekocore.block.BlockPosIterator;
import kame.nekocore.collision.ray.FilterType;
import kame.nekocore.collision.ray.HitType;
import kame.nekocore.collision.ray.Ray;
import kame.nekocore.collision.ray.RayBuilder;
import kame.nekocore.collision.ray.RayHit;

public abstract class RayUtils {
	static RayUtils utils = Init();
	
	public static RayUtils Init() {
		try {
			return (RayUtils) Class.forName("kame.NekoCore.baseutils.ray.V" + Main.VERSION).newInstance();
		}catch (Exception e) {
			Bukkit.getLogger().log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
	}

	public abstract boolean trace(Collection<? super BlockRayHit> results, World world, Vector start, Vector end, Iterator<BlockPos> pos);
	
	public abstract boolean trace(Collection<? super EntityRayHit> results, World world, Vector start, Vector end, Entity entity);

	public static abstract class RayBuilderAbstract implements RayBuilder {
		protected Map<Predicate<Block>, FilterType> block = new LinkedHashMap<>();
		protected Map<Predicate<Entity>, FilterType> entity = new LinkedHashMap<>();

		private static final Predicate<Block> Air = Block::isEmpty;
		private static final Predicate<Block> AirOrLiquid = Air.or(Block::isLiquid);


		public RayBuilder addBlockFilter(Predicate<Block> filter, FilterType type) {
			block.put(filter, type);
			return this;
		}

		public RayBuilder addEntityFilter(Predicate<Entity> filter, FilterType type) {
			entity.put(filter, type);
			return this;
		}

		public RayBuilder useTemplateFilter() {
			block.put(AirOrLiquid, FilterType.SKIP);
			return this;
		}

		public RayBuilder skipBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.SKIP);
			return this;
		}

		public RayBuilder includeBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.INCLUDE);
			return this;
		}

		public RayBuilder stopBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.STOP);
			return this;
		}

		public RayBuilder skipEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.SKIP);
			return this;
		}

		public RayBuilder includeEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.INCLUDE);
			return this;
		}

		public RayBuilder stopEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.STOP);
			return this;
		}

		public Ray traceBlock(Location from, double distance) {
			return traceBlock(from.getWorld(), from.toVector(), from.toVector().add(from.getDirection().multiply(distance)), null, this.block);
		}

		public Ray traceEntity(Location from, double distance) {
			return traceEntity(from.getWorld(), from.toVector(), from.toVector().add(from.getDirection().multiply(distance)), null, this.entity);
		}

		public Ray traceWorld(Location from, double distance) {
			return traceWorld(from.getWorld(), from.toVector(), from.toVector().add(from.getDirection().multiply(distance)), null, this.block, this.entity);
		}

		public Ray traceBlock(LivingEntity entity, double distance) {
			Location from = entity.getEyeLocation();
			return traceBlock(from.getWorld(), from.toVector(), from.toVector().add(from.getDirection().multiply(distance)), entity, this.block);
		}

		public Ray traceEntity(LivingEntity entity, double distance) {
			Location from = entity.getEyeLocation();
			return traceEntity(from.getWorld(), from.toVector(), from.toVector().add(from.getDirection().multiply(distance)), entity, this.entity);
		}

		public Ray traceWorld(LivingEntity entity, double distance) {
			Location from = entity.getEyeLocation();
			return traceWorld(from.getWorld(), from.toVector(), from.toVector().add(from.getDirection().multiply(distance)), entity, this.block, this.entity);
		}
		
		protected boolean rayTraceBlock(Collection<RayHit> results, World world, Vector from, Vector to, BlockPosIterator iterator) {
			return utils.trace(results, world, from, to, iterator);
		}
		
		protected boolean rayTraceEntity(Collection<RayHit> results, World world, Vector from, Vector to, Entity entity) {
			return utils.trace(results, world, from, to, entity);
		}
		
		protected abstract Ray traceBlock(World world, Vector from, Vector to, Entity entity, Map<Predicate<Block>, FilterType> filter);

		protected abstract Ray traceEntity(World world, Vector from, Vector to, Entity entity, Map<Predicate<Entity>, FilterType> filter);

		protected abstract Ray traceWorld(World world, Vector from, Vector to, Entity entity, Map<Predicate<Block>, FilterType> bfilter, Map<Predicate<Entity>, FilterType> efilter);
	}

	public static class BlockRayHit extends RayHit {
		private final BlockFace face;

		public BlockRayHit(Vector from, Vector hitloc, Block block, BlockFace face) {
			super(from, hitloc, block);
			this.face = face;
		}

		/**
		 * 衝突したブロックを返します。
		 * @return - ブロック
		 */
		public Block getBlock() {
			return getObject(Block.class);
		}

		@Override
		public Location getLocation() {
			return hitloc.toLocation(getBlock().getWorld());
		}

		@Override
		public BlockFace getDirection() {
			return face;
		}

		@Override
		public HitType getType() {
			return HitType.BLOCK;
		}

		@Override
		public String toString() {
			return "BlockRayHit:{Pos:{x=" + (float)hitloc.getX() + ", y=" + (float)hitloc.getY() + ", z=" + (float)hitloc.getZ() + "}, Block=" + getBlock().getType() + ", Face=" + face + "}";
		}
	}

	public static class EntityRayHit extends RayHit {
		private final BlockFace face;

		public EntityRayHit(Vector from, Vector hitloc, Entity entity, BlockFace face) {
			super(from, hitloc, entity);
			this.face = face;
		}

		/**
		 * 衝突したエンティティを返します。
		 * @return - エンティティ
		 */
		public Entity getEntity() {
			return getObject(Entity.class);
		}

		@Override
		public Location getLocation() {
			return hitloc.toLocation(getEntity().getWorld());
		}

		@Override
		public BlockFace getDirection() {
			return face;
		}

		@Override
		public HitType getType() {
			return HitType.ENTITY;
		}

		@Override
		public String toString() {
			return "EntityRayHit:{Pos:{x=" + (float)hitloc.getX() + ", y=" + (float)hitloc.getY() + ", z=" + (float)hitloc.getZ() + "}, Entity=" + getEntity().getType() + ", Face=" + face + "}";
		}
	}
}
