package kame.nekocorex.nms.collision;

import java.util.Collection;
import java.util.Iterator;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftEntity;
import org.bukkit.util.Vector;

import kame.nekocore.block.BlockPos;
import kame.nekocorex.util.RayUtils;
import net.minecraft.server.v1_10_R1.AxisAlignedBB;
import net.minecraft.server.v1_10_R1.BlockPosition;
import net.minecraft.server.v1_10_R1.Entity;
import net.minecraft.server.v1_10_R1.EnumDirection;
import net.minecraft.server.v1_10_R1.MovingObjectPosition;
import net.minecraft.server.v1_10_R1.Vec3D;
import net.minecraft.server.v1_10_R1.WorldServer;

public class V1_10_R1 extends RayUtils {
	V1_10_R1() { }
	
	/* TODO 動作のチェックが完了次第コメント部分消す
	private double[] cache = new double[0];
	public Object[] trace(World world, int x, int y, int z, double[] points) {
		WorldServer w = ((CraftWorld)world).getHandle();
		BlockPosition bp = new BlockPosition(x, y, z);
		IBlockData data = w.getType(bp);
		Vec3D a = cache == points ? cache_0 : (cache_0 = new Vec3D(cache[0], cache[1], cache[2]));
		Vec3D b = cache == points ? cache_1 : (cache_1 = new Vec3D(cache[3], cache[4], cache[5]));
		MovingObjectPosition p =  data.a(w, bp, a, b);
		return p == null ? new Object[0] : new Object[] {p.pos.x, p.pos.y, p.pos.z, toBukkit(p.direction)};
	}

	public boolean trace(Set<? super EntityRayHit> results, World world, Vector start, Vector end, Map<Predicate<org.bukkit.entity.Entity>, FilterType> filters) {
		WorldServer ws = ((CraftWorld)world).getHandle();
		AxisAlignedBB aabb = new AxisAlignedBB(start.getX(), start.getY(), start.getZ(), end.getX(), end.getY(), end.getZ());
		Vec3D vecstart = new Vec3D(start.getX(), start.getY(), start.getZ()), vecend = new Vec3D(end.getX(), end.getY(), end.getZ());
		Set<MovingObjectPosition> set = new TreeSet<>((x, y)-> Double.compare(distance(x.pos, start), distance(y.pos, start)));
		ws.getEntities(null, aabb, x -> {
			MovingObjectPosition mov = x.getBoundingBox().b(vecstart, vecend);
			if(mov != null) {
				mov.entity = x;
				set.add(mov);
			}
			return false;
		});
		for(MovingObjectPosition mov : set) {
			//FilterType last = RayUtils.check(mov.entity.getBukkitEntity(), filters);
			//if(last != FilterType.SKIP)results.add(new EntityRayHit(start, new Vector(mov.pos.x, mov.pos.y, mov.pos.z), mov.entity.getBukkitEntity(), toBukkit(mov.direction)));
			//if(last == FilterType.STOP)return true;
		}
		return false;
	}

	private Vec3D cache_0;
	private Vec3D cache_1;
	
	public Object[] tracea(World world, int x, int y, int z, double[] points) {
		WorldServer w = ((CraftWorld)world).getHandle();
		BlockPosition bp = new BlockPosition(x, y, z);
		IBlockData data = w.getChunkAtWorldCoords(bp).getBlockData(bp);
		if(data.c(w, bp) != null) {
			Vec3D vec0;
			Vec3D vec1;
			if(cache == points) {
				vec0 = cache_0;
				vec1 = cache_1;
			}else {
				cache = points;
				vec0 = cache_0 = new Vec3D(points[0], points[1], points[2]);
				vec1 = cache_1 = new Vec3D(points[3], points[4], points[5]);
			}
			MovingObjectPosition p =  data.a(w, bp, vec0, vec1);	//階段等の複数のボックス判定用
			return p == null ? new Object[0] : new Object[] {p.pos.x, p.pos.y, p.pos.z, toBukkit(p.direction)};
		}
		return null;
	}

	private double distance(Vec3D vec, Vector loc) {
		return NumberConversions.square(vec.x - loc.getX()) + NumberConversions.square(vec.y - loc.getY()) + NumberConversions.square(vec.z - loc.getZ());
	}
	*/
	
	@Override
	public boolean trace(Collection<? super BlockRayHit> results, World world, Vector start, Vector end, Iterator<BlockPos> iterator) {
		WorldServer ws = ((CraftWorld)world).getHandle();
		Vec3D a = new Vec3D(start.getX(), start.getY(), start.getZ());
		Vec3D b = new Vec3D(end.getX(), end.getY(), end.getZ());
		for(BlockPos pos = iterator.next(); pos != null; pos = iterator.next()) {
			BlockPosition bp = new BlockPosition(pos.x, pos.y, pos.z);
			MovingObjectPosition p =  ws.getType(bp).a(ws, bp, a, b);
			if(p != null) {
				Block block = world.getBlockAt(bp.getX(), bp.getY(), bp.getZ());
				Vector hit = new Vector(p.pos.x, p.pos.y, p.pos.z);
				results.add(new BlockRayHit(start, hit, block, toBukkit(p.direction)));
			}
		}
		return !results.isEmpty();
	}
	
	@Override
	public boolean trace(Collection<? super EntityRayHit> results, World world, Vector start, Vector end, org.bukkit.entity.Entity entity) {
		WorldServer ws = ((CraftWorld)world).getHandle();
		Vec3D a = new Vec3D(start.getX(), start.getY(), start.getZ());
		Vec3D b = new Vec3D(end.getX(), end.getY(), end.getZ());
		for(Entity pos : ws.getEntities(entity == null ? null : ((CraftEntity)entity).getHandle(), new AxisAlignedBB(a.x, a.y, a.z, b.x, b.y, b.z), null)) {
			MovingObjectPosition p = pos.getBoundingBox().b(a, b);
			if(p != null) {
				Vector hit = new Vector(p.pos.x, p.pos.y, p.pos.z);
				results.add(new EntityRayHit(start, hit, pos.getBukkitEntity(), toBukkit(p.direction)));
			}
		}
		return !results.isEmpty();
	}

	private BlockFace toBukkit(EnumDirection dir) {
		switch(dir) {
		case DOWN:
			return BlockFace.DOWN;
		case EAST:
			return BlockFace.EAST;
		case NORTH:
			return BlockFace.NORTH;
		case SOUTH:
			return BlockFace.SOUTH;
		case UP:
			return BlockFace.UP;
		case WEST:
			return BlockFace.WEST;
		}
		return BlockFace.SELF;
	}
}
